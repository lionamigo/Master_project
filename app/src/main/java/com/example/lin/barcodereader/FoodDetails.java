package com.example.lin.barcodereader;

import android.os.AsyncTask;
import android.widget.TextView;

import org.jsoup.Jsoup;
import org.jsoup.select.Elements;
import org.jsoup.nodes.Document;
import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class FoodDetails extends AsyncTask<String , Void , String> {


    //onPreExecute ： 執行前，一些基本設定可以在這邊做。

    //doInBackground ： 執行中，在背景做任務。

    //onProgressUpdate ： 執行中，當你呼叫publishProgress的時候會到這邊，可以告知使用者進度。

    //onPostExecute ： 執行後，最後的結果會在這邊。



    @Override
    protected String doInBackground(String... urls) {
        try
        {
            String url = urls[0];
            Document document = Jsoup.connect(url).get();
            System.out.print(parseHtml(document));

        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }


    //@Override
    protected void onPostExecute(Void result) {
        // Set title into TextView

    }

    private String parseHtml(Document document){

        Elements elements = document.select("talbe[class=data_table] > thead > tr");
        String result;
        result = elements.select("th").text();
        return result;
    }
    /*
    private ArrList<LotteryData> parseHtml(Document document) {
        Elements elements = document.select("table[class=historylist] > tbody > tr");
        List<LotteryData> list = new ArrayList<>();
        LotteryData data;
        for (Element element : elements) {
            data = new LotteryData();
            data.setNum(element.select("td > a").text());//开奖期数
            data.setDate(element.select("td").get(1).text());//开奖日期
            Elements ems = element.select("td[class=balls] em");//开奖结果
            StringBuilder sb = new StringBuilder();
            for (Element em : ems) {
                sb.append(em.text());
            }
            data.setBalls(sb.toString());
            list.add(data);
        }

        return list;
    }
    */




}
