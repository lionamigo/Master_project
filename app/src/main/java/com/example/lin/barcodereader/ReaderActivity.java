package com.example.lin.barcodereader;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import org.jsoup.nodes.Document;


import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class ReaderActivity extends AppCompatActivity {

    String url = "https://world.openfoodfacts.org/product/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reader);
        Button button = findViewById(R.id.button);
        Button web_cap = findViewById(R.id.web_cap);
        Button toturialBtn = findViewById(R.id.toturialBtn);
        final TextView t2 = findViewById(R.id.retrieveTV);
        final Activity activity = this;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentIntegrator integrator = new IntentIntegrator(activity);
                integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
                integrator.setPrompt("Scan");
                integrator.setCameraId(0);
                integrator.setBeepEnabled(true);
                integrator.setBarcodeImageEnabled(true);
                integrator.initiateScan();

            }
        });

        web_cap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Title().execute();
            }
        });

        toturialBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(ReaderActivity.this,showToturial.class);
                startActivity(intent);

            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent date){

        IntentResult result = IntentIntegrator.parseActivityResult(requestCode,resultCode,date);

        TextView t = findViewById(R.id.text);
        if(result!=null){
            if(result.getContents()==null){
                Toast.makeText(this,result.getContents(),Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(this,result.getContents(),Toast.LENGTH_LONG).show();
            }
            url = "https://world.openfoodfacts.org/product/" + result.getContents();


            //FoodDetails f;
            //    new FoodDetails().execute(url);
            t.setText(url);
        }else{
            super.onActivityResult(requestCode,resultCode,date);
        }
    }


    private class Title extends AsyncTask<Void, Void, Void> {
        String data = new String();
        //String product_name = new String();

        /*
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(MainActivity.this);
            mProgressDialog.setTitle("Android Basic JSoup Tutorial");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }
        */

        @Override
        protected Void doInBackground(Void... params) {
            /* Connect to the web site */

            Document doc = null;
            //ArrayList<String> name = new ArrayList<String>();
           // ArrayList<String> weight = new ArrayList<String>();
            int size = 0;

            try {
                doc = Jsoup.connect(url).get();
                Elements trs = doc.select("table.data_table tr");
                trs.remove(0);

                //product_name = doc.select();




                for (Element tr : trs) {

                    Elements tds = tr.getElementsByTag("td");
                    Element td = tds.first();
                    Element td2 = tds.get(1);
                   // name.add(td.text());
                  //  weight.add(td2.text());

                    data += td.text() +"    "+ td2.text() + '\n';
                    //size++;
                }



                //data += doc.getElementsByClass("data_table").select("td").first().text() +
                //doc.getElementsByClass("data_table").select("td").get(1).text() + "\n";


            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // Set title into TextView
            TextView setretrieve = findViewById(R.id.retrieveTV);
            setretrieve.setText(data);
        }
    }

}
