package com.example.lin.barcodereader;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import  android.database.sqlite.SQLiteOpenHelper;

public class Food_Database extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "Fooddetails.db";
    public static final String TABLE_NAME = "Foodtable";
    public static final String BARCODE = "BARCODE";
    public static final String NAME = "NAME";
    public static final String WEIGHT = "WEIGHT";
    public static final String SUGAR = "SUGAR";
    public static final String FAT = "FAT";
    public static final String GLYCEMIC_INDEX = "GLYCEMIC_INDEX";
    public static final String INSULIN_INDEX = "INSULIN_INDEX";


    public Food_Database(Context context) {
        super(context, DATABASE_NAME, null, 1);
        SQLiteDatabase db = this.getWritableDatabase();

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table "+ DATABASE_NAME + "(BARCODE INTEGER PRIMARY KEY AUTOINCREMENT, NAME TEXT, WEIGHT FLOAT," +
                "SUGAR FLOAT, FAT FLOAT, GLCEMIC_INDEX FLOAT, INSULIN_INDEX FLOAT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_NAME);
        onCreate(db);
    }
}
